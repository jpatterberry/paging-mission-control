package com.satellite;

import  org.junit.jupiter.api.BeforeEach;
import  org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SatelliteAlerterTest {

    private SatelliteAlerter satAlerter;
    private List<TelemetryInput> telemetryInputs = new ArrayList<TelemetryInput>();

    @BeforeEach
    void setUp() {
        satAlerter = new SatelliteAlerter();
    }

    @Test
    void testTelemetryWorkflow()
    {
        ArrayList<String> tInfo = satAlerter.getTelemetryData("telemetryData.txt");
        ArrayList<TelemetryInput> telemetryInfo = satAlerter.storeTelemetry(tInfo);
        satAlerter.processTelemetry(telemetryInfo);
    }

    @Test
    void testGetTelemetryData()
    {
        ArrayList<String> tInfo = satAlerter.getTelemetryData("telemetryData.txt");
        for (String tdata : tInfo)
        {
            System.out.println(tdata);
        }
    }

    @Test
    void testProcessAlert()
    {
        createTelemetryInputs();

        ArrayList<TelemetryInput> inputs = new ArrayList<TelemetryInput>();
        inputs.addAll(telemetryInputs);

        satAlerter.processAlert(inputs);
    }

    void createTelemetryInputs()
    {
        telemetryInputs = Arrays.asList(
                new TelemetryInput(LocalDateTime.now(), "1200", 90, 70, 80, 40, Float.valueOf("85.6"), "TSTAT"),
                new TelemetryInput(LocalDateTime.now().plusMinutes(1), "1200", 90, 70, 80, 40, Float.valueOf("91.5"), "TSTAT"),
                new TelemetryInput(LocalDateTime.now().plusMinutes(2), "1200", 90, 70, 80, 40, Float.valueOf("92.6"), "TSTAT"),
                new TelemetryInput(LocalDateTime.now().plusMinutes(3), "1200", 90, 70, 80, 40, Float.valueOf("94.8"), "TSTAT")
       );
    }
}