package com.satellite;

import java.time.LocalDateTime;

/**
 * Class that aligns with telemetry data.  A comparator is used to sort the input
 * by date
 */
public class TelemetryInput implements Comparable<TelemetryInput>
{
    private LocalDateTime recordStamp;
    private String satelliteId;
    private Integer redHigh;
    private Integer redLow;
    private Integer yellowHigh;
    private Integer yellowLow;
    private Float rawData;
    private String component;

    public TelemetryInput() {}

    public TelemetryInput(LocalDateTime recordStamp, String satelliteId, Integer redHigh, Integer redLow, Integer yellowHigh, Integer yellowLow, Float rawData, String component) {
        this.recordStamp = recordStamp;
        this.satelliteId = satelliteId;
        this.redHigh = redHigh;
        this.redLow = redLow;
        this.yellowHigh = yellowHigh;
        this.yellowLow = yellowLow;
        this.rawData = rawData;
        this.component = component;
    }

    public LocalDateTime getRecordStamp() {
        return recordStamp;
    }

    public void setRecordStamp(LocalDateTime recordStamp) {
        this.recordStamp = recordStamp;
    }

    public String getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(String satelliteId) {
        this.satelliteId = satelliteId;
    }

    public Integer getRedHigh() {
        return redHigh;
    }

    public void setRedHigh(Integer redHigh) {
        this.redHigh = redHigh;
    }

    public Integer getRedLow() {
        return redLow;
    }

    public void setRedLow(Integer redLow) {
        this.redLow = redLow;
    }

    public Integer getYellowHigh() {
        return yellowHigh;
    }

    public void setYellowHigh(Integer yellowHigh) {
        this.yellowHigh = yellowHigh;
    }

    public Integer getYellowLow() {
        return yellowLow;
    }

    public void setYellowLow(Integer yellowLow) {
        this.yellowLow = yellowLow;
    }

    public Float getRawData() {
        return rawData;
    }

    public void setRawData(Float rawData) {
        this.rawData = rawData;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    @Override
    public int compareTo(TelemetryInput telemetryItem)
    {
        int compareTime = this.recordStamp.compareTo(telemetryItem.recordStamp);
        return compareTime;
    }

}
