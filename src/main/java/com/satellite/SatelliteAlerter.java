package com.satellite;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.ListIterator;
import java.util.StringTokenizer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Ingest satellite telemetry data from a file and output a warning if there are three
 * telemetry readings within 5 minutes from the same satellite that are beyond the high or low
 * thresholds of the satellite.
 *
 * @Author Joe Atterberry
 *
 */
public class SatelliteAlerter
{
    private final static int telemetryThreshold = 3;
    private final static int timeGapThreshold = 5;
    private final static String batteryInd = "BATT";
    private final static String thermostatInd = "TSTAT";
    private final static String redHighInd = "RED HIGH";
    private final static String redLowInd = "RED LOW";

    HashSet<String> uniqueSatellites = new HashSet<String>();

    public static void main(String[] args)
    {
        SatelliteAlerter alerter = new SatelliteAlerter();
        ArrayList<String> tdata = alerter.getTelemetryData(args[0]);
        ArrayList<TelemetryInput> telemetryInfo = alerter.storeTelemetry(tdata);
        alerter.processTelemetry(telemetryInfo);
    }

    /**
     * Read the telemetry data from the file
     *
     * @param fileName
     * @return A List of telemetry data read from the file
     */
    ArrayList<String> getTelemetryData(String fileName)
    {
        ArrayList<String> telemetryInfo = new ArrayList<>();
        try {
            Stream<String> stream = Files.lines(Paths.get(fileName));
            telemetryInfo = stream.collect(Collectors.toCollection(ArrayList::new));
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return telemetryInfo;
    }

    /**
     * Take the strings from the file and convert the strings into structures.
     *
     * @param telemetry
     * @return A list of telemetry data structures
     */
    ArrayList<TelemetryInput> storeTelemetry(ArrayList<String> telemetry)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd kk:mm:ss.SSS");
        ArrayList<TelemetryInput> telemetryItems = new ArrayList<>();

        for (String inItem : telemetry)
        {
            StringTokenizer ts = new StringTokenizer(inItem, "|");

            while (ts.hasMoreTokens())
            {
                TelemetryInput tItem = new TelemetryInput();

                String tstamp = ts.nextToken();
                LocalDateTime lTime = LocalDateTime.parse(tstamp, formatter);
                tItem.setRecordStamp(lTime);

                String satId = ts.nextToken();
                tItem.setSatelliteId(satId);
                uniqueSatellites.add(satId);

                String rHigh = ts.nextToken();
                Integer redHigh = Integer.parseInt(rHigh);
                tItem.setRedHigh(redHigh);

                String yHigh = ts.nextToken();
                Integer yellowHigh = Integer.parseInt(yHigh);
                tItem.setYellowHigh(yellowHigh);

                String yLow = ts.nextToken();
                Integer yellowLow = Integer.parseInt(yLow);
                tItem.setYellowLow(yellowLow);

                String rLow = ts.nextToken();
                Integer redLow = Integer.parseInt(rLow);
                tItem.setRedLow(redLow);

                String rawMeasure = ts.nextToken();
                tItem.setRawData(Float.valueOf(rawMeasure));

                String component = ts.nextToken();
                tItem.setComponent(component);

                telemetryItems.add(tItem);
            }
        }

        return telemetryItems;
    }

    /**
     * Separate the thermostat and battery measurements for each satellite
     * @param inputs
     */
    public void processTelemetry(ArrayList<TelemetryInput> inputs)
    {
        ArrayList<TelemetryInput> satelliteThermostatGroup = new ArrayList<TelemetryInput>();
        ArrayList<TelemetryInput> satelliteBatteryGroup = new ArrayList<TelemetryInput>();

        for (String satellite : uniqueSatellites)
        {
            for (TelemetryInput tInput : inputs)
            {
                if (satellite.equalsIgnoreCase(tInput.getSatelliteId()))
                {
                    if (tInput.getComponent().equalsIgnoreCase(thermostatInd))
                        satelliteThermostatGroup.add(tInput);
                    else if (tInput.getComponent().equalsIgnoreCase(batteryInd))
                        satelliteBatteryGroup.add(tInput);
                }
            }

            // Process the battery readings
            ArrayList<TelemetryWarning> thermostatWarnings = processAlert(satelliteThermostatGroup);

            // Process the battery readings
            ArrayList<TelemetryWarning> batteryWarnings = processAlert(satelliteBatteryGroup);

            // Combine all the measurements for printing
            ArrayList<TelemetryWarning> readings = new ArrayList<TelemetryWarning>();
            readings.addAll(thermostatWarnings);
            readings.addAll(batteryWarnings);

            ObjectMapper mapper = new ObjectMapper();
            for (TelemetryWarning reading : readings)
            {
                try {
                    String warningString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(reading);
                    System.out.println(warningString);
                }
                catch (JsonProcessingException jpe) {
                    jpe.printStackTrace();
                }
            }

            satelliteThermostatGroup.clear();
            satelliteBatteryGroup.clear();
            readings.clear();
        }
    }

    /**
     * Take group of satellite measurements either thermostat readings or battery readings and
     * check if the correct number of measurements have been taken, whether they are within
     * within the five minute threshold and if there was actually a high or low battery or thermostat
     * reading
     *
     * @param satelliteMeasurements
     * @return An Array of Warning entities
     */
    ArrayList<TelemetryWarning> processAlert(ArrayList<TelemetryInput> satelliteMeasurements)
    {
        boolean redHighTrigger = false;
        boolean redLowTrigger = false;

        // Have the proper number of measurements been taken
        if (satelliteMeasurements.size() >= telemetryThreshold)
        {
            // The timestamps have already been sorted, so we just need the time gap
            Duration timeGap = Duration.between(satelliteMeasurements.get(0).getRecordStamp(), satelliteMeasurements.get(satelliteMeasurements.size() - 1).getRecordStamp());
            long gap = Math.abs(timeGap.toMinutes());

            // Check if the gap is in range
            if (gap < timeGapThreshold)
            {
                ListIterator<TelemetryInput> highIter = satelliteMeasurements.listIterator();
                ListIterator<TelemetryInput> lowIter = satelliteMeasurements.listIterator();

                // Check for the high threshold alarm
                while (highIter.hasNext() && redHighTrigger == false)
                {
                    TelemetryInput tMeasure = highIter.next();
                    if (tMeasure.getRawData() >= tMeasure.getRedHigh()) {
                        redHighTrigger = true;
                    }
                }

                // Check for the low threshold alarm
                while (lowIter.hasNext() && redLowTrigger == false)
                {
                    TelemetryInput tMeasure = lowIter.next();
                    if (tMeasure.getRawData() <= tMeasure.getRedLow()) {
                        redLowTrigger = true;
                    }
                }
            }
        }

        DateTimeFormatter outFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'kk:mm:ss.SSS'Z'");
        ArrayList<TelemetryWarning> warnings = new ArrayList<TelemetryWarning>();

        // Create warnings based on what has been found
        if (redHighTrigger)
        {
            TelemetryInput highInput = satelliteMeasurements.get(0);

            TelemetryWarning highWarning = new TelemetryWarning();
            highWarning.setSatelliteId(highInput.getSatelliteId());
            highWarning.setSeverity(redHighInd);
            highWarning.setComponent(highInput.getComponent());

            String warningDate = highInput.getRecordStamp().format(outFormatter);
            highWarning.setTimestamp(warningDate);

            warnings.add(highWarning);
        }

        if (redLowTrigger)
        {
            TelemetryInput lowInput = satelliteMeasurements.get(0);

            TelemetryWarning lowWarning = new TelemetryWarning();
            lowWarning.setSatelliteId(lowInput.getSatelliteId());
            lowWarning.setSeverity(redLowInd);
            lowWarning.setComponent(lowInput.getComponent());

            String warningDate = lowInput.getRecordStamp().format(outFormatter);
            lowWarning.setTimestamp(warningDate);

            warnings.add(lowWarning);
        }

        return warnings;
    }
}
